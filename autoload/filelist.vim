
""""""""""""""""""""""""""""""""""""""""""""""""""
" FileList
"

" Constants

let s:WindowName = "-FileList-"

"
" Functions
"

" Displays the plugin in a default state

function! FileList#Init()
    " try to load a file list
    if (!s:loadFileList())
        " if that failed, generate one
        call s:generateFileList()
    endif

    " initialize the window
    call s:initWindow()
endfunction

" Refreshes the file list if the window is open

function! FileList#Refresh()
    " only refresh if the list is open
    if (s:focusWindow(s:WindowName))
        " re-load the file list
        call s:loadFileList()

        " re-run the last search over the new list
        call s:updateFileList()
        redraw!
    endif
endfunction

" Closes both windows of the file list and frees a bit of memory

function! FileList#Close()
    let l:bufnr = bufnr(s:WindowName)

    " only do work if the buffer exists
    if (l:bufnr != -1)
        " if our buffer is focused, just focus the previous buffer's window.
        " we'll get called again when that happens.
        if (l:bufnr == bufnr("%"))
            exec bufwinnr(s:lastBuffer) . " wincmd w"
        else
            " delete our buffer
            exec "bd " . l:bufnr

            " restore the old timeoutlen
            let &timeoutlen = s:savedTimeoutLen

            " free up some memory
            let s:fileList   = []
            let s:matches    = []
            let s:lastSearch = ""

            " remove our global autocmd
            autocmd! FileList BufEnter *
        endif
    endif
endfunction

"
" Private
"

" Calculates the cache filename for the given directory

function! s:calculateCacheFilename(dir)
    let l:escaped = substitute(a:dir,     '#',      "##",  "g")
    let l:escaped = substitute(l:escaped, '[\\/]$', "",    "g")
    let l:escaped = substitute(l:escaped, '[:\\/]', "#_#", "g")
    return g:FileList_CacheLocation . "/" . l:escaped
endfunction

" Searches for a cache file in the given directory or its ancestors

function! s:getAncestorCacheFile(dir)
    " look for cache files corresponding to directories up the tree
    let l:previous = ""
    let l:current  = substitute(a:dir, '\v[^/\\]\zs$', '/', "")

    while (l:current != l:previous && match(l:current, '[\\/]\{2}$') == -1)
        let l:cachefile = s:calculateCacheFilename(l:current)

        if (filereadable(l:cachefile))
            return { 'dir': l:current, 'cachefile': l:cachefile }
        endif

        let l:previous = l:current
        let l:current  = resolve(l:previous . "/..")
    endwhile

    return { 'dir': "", 'cachefile': "" }
endfunction

" Load a file list from one of the various potential locations

function! s:loadFileList()
    " try the named index first
    let l:file = findfile(g:FileList_NamedIndex, ";")

    if (filereadable(l:file))
        let s:fileList = readfile(l:file)
        return 1
    else
        " the named index wasn't found.  check for a cached index.
        let l:info = s:getAncestorCacheFile(getcwd())

        if (len(l:info.cachefile))
            let s:fileList = readfile(l:info.cachefile)
            return 1
        endif
    endif

    " if we made it here we failed to load a file list
    return 0
endfunction

" Generates a file list for the current directory

function! s:generateFileList()
    " if we have a named index file, use that
    let l:indexfile = findfile(g:FileList_NamedIndex, ";")

    if (filereadable(l:indexfile))
        " we want to use the named index as our cache output.  we also want to
        " use its directory as the root.
        let l:dir       = fnamemodify(l:indexfile, ":h")
        let l:cachefile = l:indexfile
    else
        " the named index wasn't found.  check for a cached index.
        let l:info = s:getAncestorCacheFile(getcwd())

        if (len(l:info.cachefile))
            let l:dir       = l:info.dir
            let l:cachefile = l:info.cachefile
        else
            " no cached index found.  just create one for cwd.
            " first make sure our cache location exists.
            if (!isdirectory(g:FileList_CacheLocation))
                call mkdir(g:FileList_CacheLocation)
            endif

            let l:dir       = getcwd()
            let l:cachefile = s:calculateCacheFilename(l:dir)
        endif
    endif

    " if we have an external cache generation command, use that
    if (exists("g:FileList_CacheGenerationCommand"))
        echo "Externally generating file list index..."

        " replace {dir} with the root and {out} with the desired output file
        let l:cmd = g:FileList_CacheGenerationCommand
        let l:cmd = substitute(l:cmd, '{dir}', escape(l:dir, '\'),       "g")
        let l:cmd = substitute(l:cmd, '{out}', escape(l:cachefile, '\'), "g")

        " run the command in the background and refresh the list when done
        let l:remote = v:progpath . " --servername " .v:servername . " --remote-expr"
        if has("win32")
            let l:refresh = " & " . l:remote . " FileList#Refresh()"
            silent exec "!start /min cmd /c " . escape(l:cmd . l:refresh, '#')
        else
            let l:refresh = " && " . l:remote . " 'FileList#Refresh()' &"
            silent exec "!" . escape(l:cmd . l:refresh, '#')
        endif

        " create a blank file list for now
        if (!exists("s:fileList"))
            let s:fileList = []
        endif
    else
        echo "Generating file list index...  CTRL-C to abort."

        " generate the index and sort the results
        let l:cache = s:generateIndex(l:dir)

        " write the index to our cache location
        call writefile(l:cache, l:cachefile)

        " set the cache as our current file list
        let s:fileList = l:cache
    endif
endfunction

" Returns a list of files from the given directory and all its children.

function! s:generateIndex(current)
    let l:results = []

    " first get all the files in the current directory
    let l:files = split(globpath(a:current, "*"), "\n")

    " iterate over the files, filtering and recursing as required
    for l:file in l:files
        " don't continue for something that already matches the disallow regex
        if (match(l:file, g:FileList_CacheRegexDisallow) == -1)
            let l:type = getftype(l:file)

            " if it's a file, see if we can add it
            if (l:type == "file")
                if (match(l:file, g:FileList_CacheRegexAllow) != -1)
                    let [l:path, l:name] = split(l:file, '.*\zs[\\/]\ze.*')
                    call add(l:results, l:name . " (" . l:path . ")")
                endif
            " if it's a directory, recurse
            elseif (l:type == "dir")
                call extend(l:results, s:generateIndex(l:file))
            endif
        endif
    endfor

    " return the results, sorted case-insensitively
    return sort(l:results, 1)
endfunction

" Focuses the window containing the given buffer name.

function! s:focusWindow(window)
    let l:focused = 0

    " get our buffer
    let l:bufnr = bufnr(a:window)

    " do we have a buffer?
    if (l:bufnr != -1)
        " yes, get the window it's in
        let l:winnr = bufwinnr(l:bufnr)

        " do we have a window?
        if (l:winnr != -1)
            " yes, give it focus
            exec l:winnr . " wincmd w"
            let l:focused = 1
        endif
    endif

    return l:focused
endfunction

" Shows the file list window based.
" Creates it if it doesn't already exist.

function! s:showWindow()
    " only create our window if we can't focus an existing one
    if (!s:focusWindow(s:WindowName))
        " save the last buffer
        let s:lastBuffer = bufnr("%")

        " create our buffer in the background
        badd `=s:WindowName`
        let l:bufnr = bufnr(s:WindowName)

        " set it as unlisted
        call setbufvar(l:bufnr, '&buflisted', 0)

        " open a window for it
        exec "silent keepalt botright vertical sbuffer " . l:bufnr

        " ensure it's set up correctly
        setlocal noswapfile
        setlocal buftype=nofile
        setlocal bufhidden=wipe
        setlocal nowrap
        setlocal foldcolumn=0
        setlocal nonumber

        " add a blank line
        call append(1, "")

        " make sure we use the right amount of space
        exec "vertical resize " . g:FileList_WindowWidth
    endif
endfunction

" Returns a list of files filtered by the given terms.

function! s:filterFileList(terms)
    " only do work if we have terms to filter on
    if (a:terms != "")
        " make the space-delimited parameter a regex
        let l:escaped = escape(a:terms, '/.\')
        let l:terms   = '\v^(.*' . substitute(l:escaped, '\v +', ')\@=(.*', "g") . ')\@='

        " filter the files based on the terms
        let l:files = copy(s:fileList)
        call filter(l:files, "v:val =~ '" . l:terms . "'")

        " try to match the actual file name, if there is one
        let l:regex = '\v\w*.\w*|\w*'
        let l:match = matchstr(a:terms, l:regex)

        " if we got a match, put the matching filenames at the top of the results
        if (l:match != "")

            " this seems to be a tad faster than the option below

            let l:match = escape(l:match, '/.\')

            let l:sorted = filter(copy(l:files), "v:val =~ '^" . l:match . "'")
            call extend(l:sorted, filter(l:files, "v:val !~ '^" . l:match . "'"))

            let l:files = l:sorted

            " this is the slower option mentioned above

            " let l:match = escape(l:match, '/.\')
            " let l:position = 0
            "
            " for l:i in range(len(l:files))
            "     if l:files[l:i] =~ "^" . l:match
            "         call insert(l:files, remove(l:files, l:i), l:position)
            "         let l:position += 1
            "     endif
            " endfor
        endif
    else
        " no search terms, use the whole list
        let l:files = s:fileList
    endif

    return l:files
endfunction

" Highlights the currently selected line

function! s:highlightSelected()
    " clear anything that's currently highlighted
    silent! syn clear FileListLine

    " match the current line
    exec 'syn match FileListLine /\%' . s:selection . 'l.*/'

    " link it to look like a search result
    hi link FileListLine Search

    " update the status line
    let l:selected = (s:page-1) * s:filesPerPage + s:selection - 2
    exec 'setlocal statusline=-\ File\ List\ -%=-\ Match:\ ' . l:selected . '/' . len(s:matches) . '\ -'
endfunction

" Displays the supplied list of files in the active buffer

function! s:drawFileList()
    " save the cursor's position
    let l:cursor = col(".")

    " clear the current file list
    silent! 3,$d _

    " only do work if there are files to display
    if (len(s:matches))
        " determine range of files to show
        let l:start = s:filesPerPage * (s:page-1)
        let l:end   = min([l:start + s:filesPerPage, len(s:matches)]) - 1

        " output our list of filtered files
        call append(2, remove(copy(s:matches), l:start, l:end))

        " ensure selection isn't past the bottom of the list
        let s:selection = min([s:selection, line("$")])
        call s:highlightSelected()
    endif

    " restore the cursor position
    call cursor(1, l:cursor)
endfunction

" Filters and displays the file list based on the current search terms

function! s:updateFileList(...)
    " if we got an argument, use that
    if (a:0)
        let l:terms = a:1
    else
        let l:terms = s:lastSearch
    endif

    " filter the files
    let s:matches = s:filterFileList(l:terms)

    " select the first file to begin with
    let s:page       = 1
    let s:selection  = 3
    let s:totalPages = (len(s:matches) / s:filesPerPage) + 1

    " display the filtered list of files
    call s:drawFileList()

    " save the last search term
    let s:lastSearch = l:terms
endfunction

" Starts a new search using the current search terms

function! s:onIdle()
    " copy the search text
    let l:search = getline(1)

    " only do work if the search term is actually different
    if (l:search != s:lastSearch)
        " perform the new search
        call s:updateFileList(l:search)
    endif
endfunction

" Moves the current file selection up or down by pages

function! s:changePage(jump, ...)
    " which page are we trying to get to
    let l:page = s:page + a:jump

    " ensure we don't overstep our bounds
    if (l:page < 1)
        " select the first line on the first page
        let s:page      = 1
        let s:selection = 3
    elseif (s:totalPages < l:page)
        " select the last line on the last page
        let s:page      = s:totalPages
        let s:selection = line("$")
    else
        " update selection if specified
        if (a:0)
            let s:selection = a:1
        endif

        " change pages as requested
        let s:page = l:page
    endif

    call s:drawFileList()
endfunction

" Moves the current file selection up or down

function! s:changeSelection(jump)
    " which file are we trying to get to
    let l:selection = s:selection + a:jump

    " ensure we don't overstep our bounds
    if (l:selection < 3)
        " we're on the first line
        " move up a page and select the last line
        call s:changePage(-1, line("$"))
    elseif (l:selection > line("$"))
        " we're on the last line
        " move down a page and select the first line
        call s:changePage(1, 3)
    else
        " change lines as requested
        let s:selection = l:selection
        call s:highlightSelected()
    endif
endfunction

" Opens the currently selected file

function! s:chooseFile(command)
    " in case the user hit enter before the timeout
    call s:onIdle()

    " copy the search text
    let l:line = getline(s:selection)

    " regex to match the file
    let l:regex = '\(\f\+\) (\(.*\))'

    " match the experssion
    let l:match = matchstr(l:line, l:regex)

    " get the file and path out of the match
    let l:path = substitute(l:match, l:regex, '\2', '')
    let l:file = substitute(l:match, l:regex, '\1', '')

    " close the file list and open the file
    call FileList#Close()
    exec a:command . " " . l:path . '/' . l:file

    " ensure we're out of insert mode
    stopinsert
endfunction

" Initializes the file list window

function! s:initWindow()
    " show and resize the list window
    call s:showWindow()

    " first two lines are for the search term
    let s:filesPerPage = winheight(0) - 2

    " display the whole list of files to start with
    call s:updateFileList("")

    " fix lame mappings
    imap <buffer> <silent> <ESC>OA  <UP>
    imap <buffer> <silent> <ESC>OB  <DOWN>
    imap <buffer> <silent> <ESC>OC  <RIGHT>
    imap <buffer> <silent> <ESC>OD  <LEFT>

    " timeoutlen does not have a local value, so we have to save and restore
    " it manually.
    let s:savedTimeoutLen = &timeoutlen
    set timeoutlen=1

    " setup mappings
    inoremap <buffer> <silent> <ESC>        <ESC>:call FileList#Close()<CR>
    inoremap <buffer> <silent> <UP>         <C-O>:call <SID>changeSelection(-1)<CR>
    inoremap <buffer> <silent> <DOWN>       <C-O>:call <SID>changeSelection(+1)<CR>
    inoremap <buffer> <silent> <PAGEUP>     <C-O>:call <SID>changePage(-1)<CR>
    inoremap <buffer> <silent> <PAGEDOWN>   <C-O>:call <SID>changePage(+1)<CR>
    inoremap <buffer> <silent> <C-HOME>     <C-O>:call <SID>changePage(-1000000000)<CR>
    inoremap <buffer> <silent> <C-END>      <C-O>:call <SID>changePage(+1000000000)<CR>
    " inoremap <buffer> <silent> <F5>         <C-O>:call <SID>generateFileList()<CR>
    inoremap <buffer> <silent> <F5>         <C-O>:call <SID>generateFileList()<CR><C-O>:call <SID>updateFileList()<CR>

    " edit/split/vsplit shortcut mappings
    exec "inoremap <buffer> <silent> " . g:FileList_EditShortcut   . " <C-O>:call <SID>chooseFile('edit')<CR>"
    exec "inoremap <buffer> <silent> " . g:FileList_SplitShortcut  . " <C-O>:call <SID>chooseFile('split')<CR>"
    exec "inoremap <buffer> <silent> " . g:FileList_VsplitShortcut . " <C-O>:call <SID>chooseFile('vsplit')<CR>"

    " update the file list every so often
    exec "setlocal updatetime=" . g:FileList_UpdateTimeout
    autocmd! CursorHoldI <buffer> call s:onIdle()

    " when another buffer is focused, close the file list
    augroup FileList
        autocmd BufEnter * call FileList#Close()
    augroup END

    " finally put us into insert mode
    startinsert
endfunction

" vim:ft=vim:ff=unix
