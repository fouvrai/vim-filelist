
""""""""""""""""""""""""""""""""""""""""""""""""""
" FileList
"

"
" Requires version 7.0 or higher
"

if v:version < 700
    echomsg "FileList requires Vim 7.0 or higher"
    finish
endif

"
" Configuration Options
"

" Name of in-tree file index
if !exists("FileList_NamedIndex")
    let g:FileList_NamedIndex = "filetags"
endif

" Location for auto-generated cache indices
if !exists("FileList_CacheLocation")
    let s:temp = $TEMP

    if (s:temp == "")
        let s:temp = $TMPDIR
    endif

    let g:FileList_CacheLocation = resolve(s:temp . "/filelist")
endif

" Regex of files/folders to allow in our cache
if !exists("FileList_CacheRegexAllow")
    let g:FileList_CacheRegexAllow = ".*"
endif

" Regex of files/folders to disallow in our cache
if !exists("FileList_CacheRegexDisallow")
    let g:FileList_CacheRegexDisallow = "/\\.$"
endif

" Width of the file list when it's opened
if !exists("FileList_WindowWidth")
    let g:FileList_WindowWidth = 70
endif

" Number of milliseconds to wait without a keystroke before updating
" the file list.
if !exists("FileList_UpdateTimeout")
    let g:FileList_UpdateTimeout = 120
endif

" Shortcut for edit command
if !exists("FileList_EditShortcut")
    let g:FileList_EditShortcut = "<CR>"
endif

" Shortcut for split command
if !exists("FileList_SplitShortcut")
    let g:FileList_SplitShortcut = "<C-S>"
endif

" Shortcut for vsplit command
if !exists("FileList_VsplitShortcut")
    let g:FileList_VsplitShortcut = "<C-V>"
endif

"
" Commands
"

command! FileList       call FileList#Init()
command! CloseFileList  call FileList#Close()

"
" Mappings
"

if !hasmapto(":FileList<CR>")
    map      <unique> <silent> <C-O>      :FileList<CR>
    inoremap <unique> <silent> <C-O> <C-O>:FileList<CR>
endif

" vim:ft=vim:ff=unix
